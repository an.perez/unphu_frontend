Pasos para ejecutar este proyecto

1) Clonar el proyecto en la carpeta deseada
2) habrir en la ruta del proyecto en la consola
3) Ejecutar el comando 'npm install'
4) Ejecutar el proyecto de BackEnd
5) En el archivo de 'environment.ts' modificar el puerto en la variable 'apiUrl' dentro de dicho archivo
6) Ejecutar el comando ng serve para levantar el servidor de angular
7) habri el navegado de preferencia chrome y ir a la url http://localhost:4200 para acceder a la aplicacion


Nota:
Es de suma importancia tener intalado angular en la maquina para ejecutar este proyecto de forma ejectiva