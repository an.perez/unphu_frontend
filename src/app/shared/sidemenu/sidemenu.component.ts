import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
declare const $: any;

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {

  optionMenu = [
    {
      name: 'Mantenimiento', options: [{
        name: 'Agregar nuevo Autor', route: '/autores'
      },
      {
        name: 'Agregar nuevo Libro', route: '/books'
      },
      ],

    }
  ];

  constructor(public navService: NavbarService) { }

  ngOnInit() {
  }

  prueba(ev) {
    $('#side-menu > li').removeClass();
    ev.target.parentNode.classList.add('current-page');
  }



}
