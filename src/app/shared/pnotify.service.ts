// pnotify.service.ts
import { Injectable } from '@angular/core';
import PNotify from 'pnotify/dist/es/PNotify';
import PNotifyButtons from 'pnotify/dist/es/PNotifyButtons';
 
@Injectable()
export class PNotifyService {
  pnotify = undefined;

  constructor() {
    this.pnotify = this.getPNotify();
  }

  getPNotify() {
    PNotify.defaults.styling = 'bootstrap3'; // Bootstrap version 3
    PNotify.defaults.icons = 'bootstrap3'; // glyphicons
    PNotifyButtons; // Initiate the module. Important!
    return PNotify;
  }

  showError(error: string) {
    this.pnotify.error(error);
  }

  showSuccess(message: string) {
    this.pnotify.success(message);
  }

  showInfo(message: string) {
    this.pnotify.info(message);
  }

  showNotice(message: string) {
    this.pnotify.notice(message);
  }
}