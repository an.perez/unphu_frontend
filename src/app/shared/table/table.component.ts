import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @Input() bodydata: Observable<any[]> = new Observable<any[]> ();
  @Input() columns: Array<string> = new Array<string>();
  @Input() mostarEditar: boolean = true;
  @Input() mostarEliminar: boolean = true;

  @Output() editar: EventEmitter<any> = new EventEmitter();
  @Output() eliminar: EventEmitter<any> = new EventEmitter();

  regex = /(?=[A-Z])/;
  constructor() { }

  ngOnInit() {
  }

  Editar(object: any) {
    let copy = Object.assign({}, object)
    this.editar.emit(copy);
  }

  Eliminar(object: any) {
    this.eliminar.emit(object);
  }
}