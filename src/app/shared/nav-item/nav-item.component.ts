import { Component, OnInit, Input } from '@angular/core';
import { Options } from 'src/app/models/menu';

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.css']
})
export class NavItemComponent implements OnInit {

  @Input() navItems: Options[];
  constructor() { }

  ngOnInit() {
  }

}
