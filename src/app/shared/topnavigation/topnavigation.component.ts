import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../navbar.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-topnavigation',
  templateUrl: './topnavigation.component.html',
  styleUrls: ['./topnavigation.component.css']
})
export class TopnavigationComponent implements OnInit {

  nombreCompleto: string;
  constructor(public navService: NavbarService) { 
    //const usuario = JSON.parse(localStorage.getItem(environment.keyLoginLocalStorage));
    this.nombreCompleto = "Angel Perez Severino";
  }

  ngOnInit() {
  }

  logout() {
   
  }

}
