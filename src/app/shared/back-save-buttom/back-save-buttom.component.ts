import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-back-save-buttom',
  templateUrl: './back-save-buttom.component.html',
  styleUrls: ['./back-save-buttom.component.css']
})
export class BackSaveButtomComponent implements OnInit {

  @Input() routerBack = '';
  @Input() showBackButtom = true;
  @Input() disabledSaveButtom = false;
  @Input() isSaveMode = true;
  @Input() textSaveButtom = 'Guardar';

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() back: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if(this.routerBack) {
      this.showBackButtom = false;
    }
  }

  guardar() {
    this.save.emit();
  }
  
  editar() {
    this.edit.emit();
  }

  volver() {
    this.back.emit();
  }

}
