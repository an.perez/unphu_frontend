import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSaveButtomComponent } from './back-save-buttom.component';

describe('BackSaveButtomComponent', () => {
  let component: BackSaveButtomComponent;
  let fixture: ComponentFixture<BackSaveButtomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackSaveButtomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackSaveButtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
