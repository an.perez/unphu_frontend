import { Component, OnInit } from '@angular/core';
import { PNotifyService } from 'src/app/shared/pnotify.service';
import { Router } from '@angular/router';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { GeneralType } from 'src/app/models/GeneralType';
import { AutoresService } from 'src/app/services/autores.service';
import { Autores } from 'src/app/models/Autores';

@Component({
  selector: 'app-autores',
  templateUrl: './autores.component.html',
  styleUrls: ['./autores.component.css']
})
export class AutoresComponent implements OnInit {
  Generos: Array<GeneralType> = new Array<GeneralType>();
  Autor: Autores = new Autores();

  constructor(private generalServices: GeneralTypeService, private autorServices: AutoresService, private pnotifyService: PNotifyService, private router: Router) { }

  ngOnInit() {
    this.generalServices.FindByParentCode("GELI").subscribe(res => {
      this.Generos = res;
    });
  }

  AddNew() {
    if (this.validar()) { return; }

    this.autorServices.Save(this.Autor).subscribe(res => {
      if (res.Indx > 0) {
        this.pnotifyService.showSuccess('El autor a sido registrado correctamente');
      }
    });
  }

  validar() {
    let error = false;

    if (this.Autor.Nombre == undefined || this.Autor.Nombre == "") {
      this.pnotifyService.showError('el campo nombre no puede estar vacio');
      error = true;
    }

    return error;
  }

}
