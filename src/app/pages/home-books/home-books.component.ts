import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/models/Book';
import { LibrosService } from 'src/app/services/libros.service';
import { HistoricoLibros } from 'src/app/models/HistoricoLibros';

@Component({
  selector: 'app-home-books',
  templateUrl: './home-books.component.html',
  styleUrls: ['./home-books.component.css']
})
export class HomeBooksComponent implements OnInit {

  books: Array<HistoricoLibros> = new Array<HistoricoLibros>();

  constructor(private bookServices: LibrosService) { }

  ngOnInit() {
    this.bookServices.GetHistorico().subscribe(res => {
      this.books = res;
    });
  }

  Like(id: string){
    this.bookServices.Like(id).subscribe(res=>{

    });
  }

  Dislike(id: string){
    this.bookServices.Dislike(id).subscribe(res=>{
      
    });
  }
}
