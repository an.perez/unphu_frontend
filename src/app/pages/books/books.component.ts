import { Component, OnInit } from '@angular/core';
import { PNotifyService } from 'src/app/shared/pnotify.service';
import { Router } from '@angular/router';
import { AutoresService } from 'src/app/services/autores.service';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { LibrosService } from 'src/app/services/libros.service';
import { Autores } from 'src/app/models/Autores';
import { GeneralType } from 'src/app/models/GeneralType';
import { Book } from 'src/app/models/Book';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  Autores: Array<Autores> = new Array<Autores>();
  Generos: Array<GeneralType> = new Array<GeneralType>();
  Libro: Book = new Book();

  constructor(private autorServices: AutoresService
    , private generalServices: GeneralTypeService
    , private bookServices: LibrosService
    , private pnotifyService: PNotifyService
    , private router: Router) { }

  ngOnInit() {
    this.autorServices.GetAll().subscribe(res => {
      this.Autores = res;
    });

    this.generalServices.FindByParentCode("GELI").subscribe(res => {
      this.Generos = res;
    });
  }

  AddNew() {
    if (this.validar()) { return; }

    this.bookServices.Save(this.Libro).subscribe(res => {
      if (res.Indx > 0) {
        this.pnotifyService.showSuccess('El libro a sido registrado correctamente');
      }
    });
  }

  validar() {
    let error = false;

    if (this.Libro.Nombre == undefined || this.Libro.Nombre == "") {
      this.pnotifyService.showError('El campo nombre no puede estar vacio');
      error = true;
    }

    if (this.Libro.Epilogo == undefined || this.Libro.Epilogo == "") {
      this.pnotifyService.showError('El campo epilogo no puede estar en blanco');
      error = true;
    }

    if (this.Libro.Autor == undefined || this.Libro.Autor == "") {
      this.pnotifyService.showError('Debe seleccionar un autor');
      error = true;
    }

    if (this.Libro.GeneroLiterario == undefined || this.Libro.GeneroLiterario == 0) {
      this.pnotifyService.showError('Debe seleccioner un genera');
      error = true;
    }

    if (this.Libro.FechaPublicacion == undefined) {
      this.pnotifyService.showError('Debe colocar la fecha de publicacion');
      error = true;
    }

    return error;
  }

}
