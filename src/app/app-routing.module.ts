import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LayoutComponent } from './shared/layout/layout.component';
//Test
import { HomeBooksComponent } from 'src/app/pages/home-books/home-books.component';
import { BooksComponent } from 'src/app/pages/books/books.component';
import { AutoresComponent } from 'src/app/pages/autores/autores.component';

const routes: Routes = [
  // App routes goes here here

  //no layout routes
  { path: '', component: LayoutComponent },
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'home', component: HomeBooksComponent },
      { path: 'books', component: BooksComponent },
      { path: 'autores', component: AutoresComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
