import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidemenuComponent } from './shared/sidemenu/sidemenu.component';
import { TopnavigationComponent } from './shared/topnavigation/topnavigation.component';
import { FooterComponent } from './shared/footer/footer.component';
import { NavbarService } from './shared/navbar.service';
import { TableComponent } from './shared/table/table.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helpers/jwt-interceptor';
import { ErrorInterceptor } from './helpers/error-interceptor';
import { LayoutComponent } from './shared/layout/layout.component';
import { NgxMaskModule } from 'ngx-mask';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BootstrapModule } from './shared/bootstrap/bootstrap.module';

import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ModalModule } from 'ngx-bootstrap/modal';

import { PNotifyService } from './shared/pnotify.service';
import { NavItemComponent } from './shared/nav-item/nav-item.component';
import { BsDatepickerModule, defineLocale, deLocale } from 'ngx-bootstrap';
import { HomeBooksComponent } from './pages/home-books/home-books.component';
import { BooksComponent } from './pages/books/books.component';
import { AutoresComponent } from './pages/autores/autores.component';

// defineLocale('es-us', deLocale); 
@NgModule({
  declarations: [
    AppComponent,
    SidemenuComponent,
    TopnavigationComponent,
    FooterComponent,
    TableComponent,
    LayoutComponent,
    NavItemComponent,
    HomeBooksComponent,
    BooksComponent,
    AutoresComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(),
    NgxSpinnerModule,
    TypeaheadModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' })

    // BootstrapModule
  ],
  providers: [NavbarService, PNotifyService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
