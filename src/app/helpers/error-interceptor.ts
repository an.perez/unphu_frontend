import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { PNotifyService } from '../shared/pnotify.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private pnotifyService: PNotifyService, private spinner: NgxSpinnerService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                //this.authenticationService.logout();
                location.reload(true);
            }

            if (err.status === 500) {
                this.pnotifyService.showError('Ha ocurrido un error desconocido');
                this.spinner.hide();
            }
            
            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}