export class GeneralType{
    GID: string;
    Indx: number;
    Codigo: string;
    Nombre: string;
    PadreId: string;
    Estatus: boolean;
    FechaRegistro: Date;
    RegistradoPor: string;

}