export class Login {
    Token: string;
    UserName: string;
    TimeSession: number;
    Modules: Modules;
}

export class Modules {
    Role: string;
    ModuleName: string;
    Options: Array<Options>;
}

export class Options {
    OptionName: string;
    OptionLink: string;
    _select: boolean;
    _update: boolean;
    _delete: boolean;
    _insert: boolean;
    SubOptions: Array<Options>;
}
