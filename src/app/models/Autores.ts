export class Autores{
    GID: string;
    Indx: number;
    Nombre: string;
    Descripcion: string;
    GeneroLiterario: number;
    Estatus: boolean;
    FechaRegistro: Date;
    RegistradoPor: string;

}