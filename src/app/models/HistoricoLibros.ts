export class HistoricoLibros{
    GID: string;
    Libro: string;
    Epilogo: string;
    Autor: string;
    FechaPublicacion: Date;
    ClasificacionTotal: number;
    VotosAFavor: string;
    VotosEnContra: string;

}