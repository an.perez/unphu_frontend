export class Book{
    GID: string;
    Indx: number;
    Nombre: string;
    Epilogo: string;
    Autor: string;
    GeneroLiterario: number;
    FechaPublicacion: Date;
    ClasificacionTotal: number;
    VotosAFavor: string;
    VotosEnContra: string;
    Estatus: boolean;
    FechaRegistro: Date;
    RegistradoPor: string;

}