import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from '../models/Book';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LibrosService {
  constructor(private http: HttpClient) { }

  GetHistorico(){
    return this.http.get<any>(`${environment.apiUrl}book/GetHistorico`);
  }
  GetAll() {
    return this.http.get<any>(`${environment.apiUrl}book/GetAll`);
  }

  FindById(id: string) {

    return this.http.get<Book>(`${environment.apiUrl}book/FindById/${id}`);
  }

  FindByName(name: string) {

    return this.http.get<Book[]>(`${environment.apiUrl}book/FindByName?name=${name}`);
  }

  Like(id: string) {

    return this.http.get<any>(`${environment.apiUrl}book/Like/${id}`);
  }

  Dislike(id: string) {

    return this.http.get<Book[]>(`${environment.apiUrl}book/Dislike/${id}`);
  }

  Save(book: Book) {
    return this.http.post<any>(`${environment.apiUrl}book/Add`, book);
  }
}
