import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Autores } from '../models/Autores';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  constructor(private http: HttpClient) { }

  GetAll() {
    return this.http.get<any>(`${environment.apiUrl}autor/GetAll`);
  }

  FindById(id: string) {

    return this.http.get<Autores>(`${environment.apiUrl}autor/FindById?id=${id}`);
  }

  FindByName(name: string) {

    return this.http.get<Autores[]>(`${environment.apiUrl}autor/FindByParentCode?name=${name}`);
  }

  Save(autor: Autores) {
    return this.http.post<any>(`${environment.apiUrl}autor/Add`, autor);
  }
}
