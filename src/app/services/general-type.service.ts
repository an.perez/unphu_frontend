import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralType } from '../models/GeneralType';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralTypeService {

  constructor(private http: HttpClient) { }

  GetAll() {
    return this.http.get<any>(`${environment.apiUrl}general/GetAll`);
  }

  FindById(id: string) {

    return this.http.get<GeneralType>(`${environment.apiUrl}general/FindById?id=${id}`);
  }

  FindByParentCode(parentCode: string) {

    return this.http.get<GeneralType[]>(`${environment.apiUrl}general/FindByParentCode?parentCode=${parentCode}`);
  }

  Save(generalType: GeneralType) {
    return this.http.post<any>(`${environment.apiUrl}general/Add`, generalType);
  }
}
